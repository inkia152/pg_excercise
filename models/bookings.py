from sqlalchemy import Column, Integer, DateTime

from app import db


class Booking(db.Model):

    __tablename__ = 'bookings'
    bookid = Column(Integer, primary_key=True)
    facid = Column(Integer)
    memid = Column(Integer)
    starttime = Column(DateTime(timezone=False))
    slots = Column(Integer)

    def __repr__(self):
        return f'<{self.__class__.__name__} (facid: {self.facid} memid: {self.memid})>'