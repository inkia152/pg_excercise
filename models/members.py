from sqlalchemy import Column, Integer, String, DateTime

from app import db


class Member(db.Model):
    __tablename__ = 'members'
    memid = Column(Integer, primary_key=True)
    surname = Column(String(200))
    firstname = Column(String(200))
    address = Column(String(300))
    zipcode = Column(Integer)
    telephone = Column(String(20))
    recommendedby = Column(Integer)
    joindate = Column(DateTime(timezone=False))

    def __repr__(self):
        return f'<{self.__class__.__name__} (id: {self.memid})>'