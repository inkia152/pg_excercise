from sqlalchemy import Column, Integer, String, Numeric

from app import db


class Facility(db.Model):
    __tablename__ = 'facilities'
    facid = Column(Integer, primary_key=True)
    name = Column(String(100))
    membercost = Column(Numeric)
    guestcost = Column(Numeric)
    initialoutlay = Column(Numeric)
    mounthlymaintenance = Column(Numeric)

    def __repr__(self):
        return f'<{self.__class__.__name__} (id: {self.facid}, name: {self.name})>'

