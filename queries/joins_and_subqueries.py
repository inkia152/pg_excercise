# coding: utf-8
from sqlalchemy import and_, literal_column, case, func, desc
from sqlalchemy.orm import aliased

from app import db
from models import Facility, Member, Booking

'''
Retrieve the start times of members' bookings
SQL:
select bks.starttime from cd.bookings bks
inner join cd.members mems on mems.memid = bks.memid
where mems.firstname='David' and mems.surname='Farrell';
OR:
select bks.starttime from cd.bookings bks, cd.members mems
where mems.firstname='David' and mems.surname='Farrell' and mems.memid = bks.memid;
'''
db.sesion.query(Booking.starttime).join(Member, Member.memid == Booking.memid).\
                filter(Member.firstname == 'David', Member.surname == 'Farrell').all()

'''
Work out the start times of bookings for tennis courts
SQL:
select cd.bookings.starttime as start, cd.facilities.name from cd.bookings 
join cd.facilities on cd.facilities.facid = cd.bookings.facid 
where cd.facilities.facid in (0,1) and cd.bookings.starttime >= '2012-09-21' and cd.bookings.starttime < '2012-09-22'
order by cd.bookings.starttime;
'''
db.session.query(Booking.starttime, Facility.name).join(Facility, Facility.facid == Booking.facid).\
                filter(and_(Facility.facid.in_([1, 2]), Booking.starttime >='2012-09-21', Booking.starttime < '2012-09-22')).\
                order_by(Booking.starttime).all()

'''
Produce a list of all members who have recommended another member
SQL:
select distinct recs.firstname, recs.surname
from cd.members mems inner join cd.members recs	on recs.memid = mems.recommendedby
order by surname, firstname;
'''
mems = aliased(Member)
recs = aliased(Member)
db.session.query(recs.firstname, recs.surname).distinct().join(mems, mems.memid == recs.recommendedby).\
                 order_by(recs.surname, recs.firstname).all()

'''
Produce a list of all members, along with their recommender
SQL:
select mems.firstname as memfname, mems.surname as memsname, recs.firstname as recfname, recs.surname as recsname  
from cd.members mems 
left outer join cd.members recs 
on recs.memid = mems.recommendedby
order by memsname, memfname;
'''
db.session.query(mems.firstname, mems.surname, recs.firstname, recs.surname).\
                 outerjoin(recs, recs.memid == mems.recommendedby).\
                 order_by(mems.surname, mems.firstname).all()

'''
Produce a list of all members who have used a tennis court
SQL:
select distinct mems.firstname || ' ' || mems.surname as member, facs.name as facility
from cd.members mems 
join cd.bookings bks on mems.memid = bks.memid
join cd.facilities facs on bks.facid = facs.facid
where bks.facid in (0,1)
order by member
'''