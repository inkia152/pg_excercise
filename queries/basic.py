# coding: utf-8
from sqlalchemy import and_, literal_column, case, func, desc

from app import db
from models import Facility, Member

# https://www.pgexercises.com

'''
Retrieve everything from a table
SQL:
select * from cd.facilities;
'''
db.session.query(Facility).all()

'''
Retrieve specific columns from a table
SQL:
select name, membercost from cd.facilities
'''
db.session.query(Facility.name, Facility.membercost).all()

'''
Control which rows are retrieved
SQL:
select * from cd.facilities where membercost > 0;
'''
db.session.query(Facility).filter(Facility.membercost > 0).all()

'''
Control which rows are retrieved - part 2
SQL:
select facid, name, membercost, monthlymaintenance from cd.facilities where membercost < monthlymaintenance /50.0 and membercost > 0
'''
db.session.query(Facility.facid, Facility.name, Facility.membercost, Facility.mounthlymaintenance).\
    filter(and_(Facility.membercost < Facility.mounthlymaintenance / 50.0, Facility.membercost > 0)).all()

'''
Basic string searches
SQL:
select * from cd.facilities where name like '%Tennis%'
'''
db.session.query(Facility).filter(Facility.name.like("%Tennis%")).all()

'''
Matching against multiple possible values
SQL:
select * from cd.facilities where facid in (1,5)
'''
db.session.query(Facility).filter(Facility.facid.in_([1, 5])).all()

'''
Classify results into buckets
SQL: 
select name, case when monthlymaintenance > 100 then 'expensive' else 'cheap' end as cost  from cd.facilities 
'''
db.session.query(Facility.name, case([(Facility.mounthlymaintenance > 100, literal_column("'expensive'"))],
                                     else_=literal_column("'cheap'")).label('cost')).all()

'''
Working with dates
SQL:
select memid, surname, firstname, joindate from cd.members where joindate > date('2012-09-01')
'''
db.session.query(Member.memid, Member.surname, Member.firstname, Member.joindate).filter(Member.joindate > '2012-09-01').all()

'''
Removing duplicates, and ordering results
SQL:
select distinct surname from cd.members ORDER BY surname asc  limit 10
'''
db.session.query(Member.surname).distinct().order_by(Member.surname.asc()).limit(10).all()

'''
Combining results from multiple queries
SQL:
select surname from cd.members union select name from cd. facilities
'''
q1 = db.session.query(Member.surname)
q2 = db.session.query(Facility.name)
q1.union(q2).all()

'''
Simple aggregation
SQL:
select max(joindate) from cd.members
'''
db.session.query(func.max(Member.joindate)).all()

'''
More aggregation
SQL:
1)select firstname, surname, joindate from cd.members	where joindate = (select max(joindate) from cd.members);
2)select firstname, surname, joindate	from cd.members order by joindate desc limit 1;
'''
db.session.query(Member.firstname, Member.surname, Member.joindate).\
                 filter(Member.joindate == db.session.query(func.max(Member.joindate))).all()
db.session.query(Member.firstname, Member.surname, Member.joindate).\
                 order_by(desc(Member.joindate)).limit(1).all()
