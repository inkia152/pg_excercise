# coding: utf-8
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, instance_relative_config=True)
app.config.from_mapping(
    SECRET_KEY='dev',
    SQLALCHEMY_DATABASE_URI='postgresql://pguser:1111@localhost/pgexercises'

)
db = SQLAlchemy()
db.init_app(app)

from models import User, Facility, Member, Booking